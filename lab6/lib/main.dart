import 'dart:math';

import 'package:flutter/material.dart';

void main() => runApp(AnimatedContainerApp());

class AnimatedContainerApp extends StatefulWidget {
  @override
  _AnimatedContainerAppState createState() => _AnimatedContainerAppState();
}

class _AnimatedContainerAppState extends State<AnimatedContainerApp> {
  bool _loading = false;
  String _swQuote = "Hello There.";
  double _opacity = 1.0;
  double _width = 50;
  double _height = 50;
  Color _color = Colors.green;
  BorderRadiusGeometry _borderRadius = BorderRadius.circular(8);

  String _getNewQuote() {
    var quotes = ["This Is Where The Fun Begins.", "There's Always A Bigger Fish.", "A Surprise To Be Sure But A Welcome One.",
                  "A Fine Addition To My Collection.", "Now This Is Podracing.", "Only A Sith Deals In Absolutes.", "Hello There.",
                  "I Am The Senate.", "It's Over, Anakin. I Have The High Ground."];
    return quotes[Random().nextInt(quotes.length - 1)];
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Starwars Quotes'),
        ),
        body: Center(
            child: AnimatedOpacity(
            opacity: _opacity,
            duration: Duration(seconds: 2),
            child: AnimatedContainer(
                child: Center(
                  child: Container(
                      padding: EdgeInsets.all(10),
                      child: Text(_swQuote, style: TextStyle(fontSize: 20, fontWeight: FontWeight.w900,),)
                  ),
                ),
                width: _width,
                height: _height,
                decoration: BoxDecoration(
                  color: _color,
                  //you can change this to a single solid color to verify that the randomly changing opacity is working
                  borderRadius: _borderRadius,
                ),
                duration: Duration(seconds: 2),
                curve: Curves.easeOutSine,
                onEnd: () {
                  setState(() {
                    _swQuote = _getNewQuote();
                    _loading = false;
                  });
                }),
          )),
        floatingActionButton: FloatingActionButton(
          child: _loading
              ? SizedBox(
                  child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                    strokeWidth: 5.0,
                  ),
                )
              : Icon(Icons.play_arrow),
          onPressed: () {
            setState(() {
              final random = Random();
              _loading = true;
              _opacity = (random.nextInt(50).toDouble() + 50) / 100;
              _width = random.nextInt(300).toDouble() + 75;
              _height = random.nextInt(450).toDouble() + 75;

              _color = Color.fromRGBO(
                random.nextInt(256),
                random.nextInt(256),
                random.nextInt(256),
                1,
              );

              _borderRadius =
                  BorderRadius.circular(random.nextInt(100).toDouble());
            });
          },
        ),
      ),
    );
  }
}
